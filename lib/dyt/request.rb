module DYT
  ##
  # Provide a general interface for send requests to YouTube Data API endpoint.
  class Request
    attr_reader :response

    def initialize(endpoint, **defaults)
      @endpoint = endpoint
      @defaults = merge_with_defaults(defaults)

      @response = nil
    end

    def perform(method, **parameters)
      url = File.join(@endpoint, method)
      @response = @defaults[:http_processor].get(url, parameters)

      jsonify_response
    rescue StandardError
      raise 'The performed request is invalid'
    end

    def ok?
      @response['error'].nil?
    end

    private

    def merge_with_defaults(defaults)
      { json_parser: Oj, http_processor: Curl }.merge defaults
    end

    def jsonify_response
      @response = @defaults[:json_parser].load(@response.body_str)
    end
  end
end
