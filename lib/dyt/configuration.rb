module DYT
  class Configuration
    attr_reader :options

    def initialize(directory = './config', **defaults)
      @defaults = sanitize_defaults(defaults)
      @directory = obtain_absolute_path(directory)
      @options = {}

      check_for_existing_file
    end

    def load
      file = File.read(File.join(@directory, @defaults[:filename]))
      @options = @defaults[:parser].load(parse_erb(file))
    end

    private

    def sanitize_defaults(defaults)
      { filename: 'secrets.yml', parser: YAML, template: ERB }.merge defaults
    end

    def obtain_absolute_path(path)
      File.expand_path(path, File.dirname($PROGRAM_NAME))
    end

    def check_for_existing_file
      path = File.join(@directory, @defaults[:filename])
      raise IOError, "Unable to find #{path}" unless File.exist?(path)
    end

    def parse_erb(template)
      @defaults[:template].new(template).result
    end
  end
end
