module DYT
  class Auth
    attr_reader :credentials

    def initialize(options = {})
      @options = options
      @credentials = {}
    end

    def register
      find_developer_key
      find_oauth2_credentials

      if @credentials.empty?
        raise 'No developer key or OAuth2 credentials defined'
      end
    end

    private

    def find_developer_key
      dev_key = @options['dev_key']

      return unless dev_key
      @credentials = { dev_key: dev_key }
    end

    def find_oauth2_credentials
      client_id = @options['client_id']
      client_secret = @options['client_secret']

      return unless client_id && client_secret

      @credentials = { client_id: client_id,
                       client_secret: client_secret }
    end
  end
end
