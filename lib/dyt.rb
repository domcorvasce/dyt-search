# coding: utf-8

# third-party libraries
require 'curb'
require 'yaml'
require 'erb'
require 'oj'

# dyt-search core
require_relative 'dyt/configuration'
require_relative 'dyt/auth'
require_relative 'dyt/request'

module DYT
  configuration = Configuration.new
  authentication = Auth.new(configuration.load)
  authentication.register

  AUTH = authentication.credentials
end
