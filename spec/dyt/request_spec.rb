require_relative '../helpers/request_spec'
require 'rspec'

RSpec.describe DYT::Request do
  subject { described_class.new('https://www.googleapis.com/youtube/v3/') }

  context '#perform' do
    it 'does a request to the given endpoint' do
      subject.perform('search', part: 'snippet')
      expect(subject.ok?).to eq(false)
    end

    it 'raises an error if the request is invalid' do
      expect do
        subject.perform('serch', part: 'snippet')
      end.to raise_error('The performed request is invalid')
    end
  end
end
