require_relative '../helpers/auth_spec'
require 'rspec'

RSpec.describe DYT::Auth do
  context 'during registration' do
    it 'chooses developer key because no OAuth2 credentials defined' do
      authentication = described_class.new('dev_key' => 'HELLO_WORLD')
      authentication.register

      expect(authentication.credentials[:dev_key]).not_to be_nil
    end

    it 'chooses OAuth2 credentials because defined' do
      authentication = described_class.new('dev_key' => 'HELLO',
                                           'client_id' => 'EXAMPLE_CLIENT_ID',
                                           'client_secret' => 'SECR*T')
      authentication.register

      expect(authentication.credentials[:dev_key]).to be_nil
      expect(authentication.credentials[:client_id]).to eq('EXAMPLE_CLIENT_ID')
    end

    it 'raises an error because no credentials defined' do
      authentication = described_class.new(fake_key: 'FAKE_KEY')
      expect { authentication.register }.to raise_error(RuntimeError)
    end
  end
end
