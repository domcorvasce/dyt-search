require_relative '../helpers/configuration_spec'
require 'rspec'

RSpec.describe DYT::Configuration do
  context 'during initialization' do
    it 'raises an exception if file is missing' do
      expect { described_class.new('./fixtur3s') }.to raise_error(IOError)
    end
  end

  context 'during loading' do
    before do
      @config = described_class.new('./fixtures')
      @config.load
    end

    it 'fills @options instance variable' do
      expect(@config.options['codename']).to eq('Hazel')
    end

    it 'parses inline ERB' do
      expect(@config.options['operation']).to eq(100)
    end
  end
end
